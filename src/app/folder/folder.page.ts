import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-folder',
  templateUrl: './folder.page.html',
  styleUrls: ['./folder.page.scss'],
})
export class FolderPage implements OnInit {
  public folder: string;

  constructor(private activatedRoute: ActivatedRoute, private router: Router) {}

  ngOnInit() {
    this.folder = this.activatedRoute.snapshot.paramMap.get('id');
  }

  onClickProcedure() {
    this.router.navigate(['/procedure', 'asd']);
  }

  onClickFb() {
    window.open('android-app://com.google.android.youtube', '_system');
  }

  onClickFb1() {
    window.open('youtube://');
  }

  onClickFb2() {
    // this.iab.create('android-app://com.google.android.youtube', "_system");
  }
}
